package com.skillbranch.plugin.extensions.psi

import com.android.SdkConstants
import com.android.tools.idea.psi.TagToClassMapper
import com.intellij.psi.PsiClass
import com.intellij.psi.xml.XmlDocument
import com.intellij.psi.xml.XmlFile
import com.intellij.psi.xml.XmlTag
import com.skillbranch.plugin.PluginsConstants
import com.skillbranch.plugin.extensions.fromSnakeToCamelCase
import com.skillbranch.plugin.model.AndroidTagInfo
import org.jetbrains.kotlin.idea.util.module


fun XmlFile.getPageObjectClassName() = this.name.removeSuffix(PluginsConstants.XML_FILE_SUFFIX).fromSnakeToCamelCase()


fun XmlFile.collectTagsInfo(): List<AndroidTagInfo> {
    return document?.collectXmlTags()?.mapNotNull { it.toAndroidTagInfo() } ?: emptyList()
}

private fun XmlDocument.collectXmlTags(): List<XmlTag> {
    val tags = mutableListOf<XmlTag>()

    rootTag?.collectXmlTagsRecursively(tags)

    return tags
}

private fun XmlTag.collectXmlTagsRecursively(tags: MutableList<XmlTag>) {
    tags += this
    this.children.filterIsInstance<XmlTag>().forEach { it.collectXmlTagsRecursively(tags) }
}

private fun XmlTag.toAndroidTagInfo(): AndroidTagInfo? {
    val idAttr = this.attributes.find { it.localName == SdkConstants.RESOURCE_CLZ_ID }
    val idValue = idAttr?.value?.removePrefix(SdkConstants.NEW_ID_PREFIX) ?: return null

    return this.toPsiClass()?.let { psiClass ->
        AndroidTagInfo(
            id = idValue,
            androidViewDeclaration = psiClass.getClosestAndroidViewDeclaration()
        )
    }
}


private fun XmlTag.toPsiClass(): PsiClass? {
    val tagToClassMapper = this.module?.let { TagToClassMapper.getInstance(it) }
    return tagToClassMapper?.getClassMap(SdkConstants.CLASS_VIEW)?.get(localName)
}