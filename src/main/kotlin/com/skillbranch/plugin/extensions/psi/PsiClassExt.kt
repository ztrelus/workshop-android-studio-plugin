package com.skillbranch.plugin.extensions.psi

import com.intellij.psi.PsiClass
import com.intellij.psi.util.InheritanceUtil
import com.skillbranch.plugin.model.AndroidViewDeclaration


fun PsiClass.getClosestAndroidViewDeclaration(): AndroidViewDeclaration {
    return AndroidViewDeclaration.values().first { androidViewDeclaration ->
        val widgetsClasses = androidViewDeclaration.widgetsClassFQNames

        widgetsClasses.find { InheritanceUtil.isInheritor(this, it) } != null
    }
}