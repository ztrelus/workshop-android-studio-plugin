package com.skillbranch.plugin.extensions.psi

import com.intellij.psi.PsiDirectory
import com.intellij.util.IncorrectOperationException


fun PsiDirectory.checkFileCanBeCreated(fileName: String): Boolean {
    return try {
        checkCreateFile(fileName)
        true
    } catch (ex: IncorrectOperationException) {
        false
    }
}