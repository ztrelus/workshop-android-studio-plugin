package com.skillbranch.plugin.extensions

import com.android.tools.idea.templates.TemplateUtils
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiNameHelper
import com.skillbranch.plugin.PluginsConstants


fun String.toKotlinFileName(): String {
    return "${this}${PluginsConstants.KOTLIN_FILE_SUFFIX}"
}

fun String.fromSnakeToCamelCase(): String {
    return TemplateUtils.underlinesToCamelCase(this)
}

fun String.isValidIdentifier(project: Project): Boolean {
    val psiNameHelper = PsiNameHelper.getInstance(project)
    return psiNameHelper.isIdentifier(this)
}

fun String.isValidPackageName(project: Project): Boolean {
    val psiNameHelper = PsiNameHelper.getInstance(project)
    return psiNameHelper.isQualifiedName(this)
}