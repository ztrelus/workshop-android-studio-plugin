package com.skillbranch.plugin.extensions.layout

import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.Messages
import javax.swing.JComponent


fun JComponent.showErrorDialog(project: Project, message: String) {
    Messages.showMessageDialog(
        project,
        message,
        "Error!",
        Messages.getErrorIcon()
    )
    this.requestFocusInWindow()
}