package com.skillbranch.plugin.actions

import com.intellij.psi.xml.XmlFile
import com.intellij.refactoring.MoveDestination


data class CreatePageObjectDialogParams(
    val xmlFile: XmlFile,
    val className: String,
    val targetPackageName: String,
    val targetMoveDestination: MoveDestination
)