package com.skillbranch.plugin.actions

import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.psi.PsiManager
import com.intellij.psi.xml.XmlFile
import com.intellij.refactoring.MoveDestination
import com.intellij.refactoring.PackageWrapper
import com.intellij.refactoring.ui.PackageNameReferenceEditorCombo
import com.intellij.ui.RecentsManager
import com.intellij.ui.components.JBTextField
import com.intellij.ui.layout.panel
import com.skillbranch.plugin.extensions.isValidIdentifier
import com.skillbranch.plugin.extensions.isValidPackageName
import com.skillbranch.plugin.extensions.layout.showErrorDialog
import com.skillbranch.plugin.extensions.psi.checkFileCanBeCreated
import com.skillbranch.plugin.extensions.psi.getPageObjectClassName
import com.skillbranch.plugin.extensions.toKotlinFileName
import org.jetbrains.kotlin.idea.refactoring.ui.KotlinDestinationFolderComboBox
import java.awt.BorderLayout
import javax.swing.JComponent
import javax.swing.JPanel


class CreatePageObjectDialog(
    private val project: Project,
    private val xmlFile: XmlFile
) : DialogWrapper(project, true) {

    companion object {
        private const val KEY_PACKAGE_NAME_RECENTS = "com.skillbranch.plugin.actions.CreatePageObjectDialog.packageName"
    }

    private var className: String = "${xmlFile.getPageObjectClassName()}Screen"


    private lateinit var classNameTextField: JBTextField
    private lateinit var packageNameChooserComboBox: PackageNameReferenceEditorCombo
    private lateinit var destinationFolderComboBox: KotlinDestinationFolderComboBox


    init {
        init()
        title = "Create <Screen> Page Object"
    }


    // Class name
    // Package name
    override fun createNorthPanel(): JComponent? {
        return panel {
            titledRow("Enter page object class name:") {
                row {
                    classNameTextField =
                        org.jetbrains.kotlin.tools.projectWizard.wizard.ui.textField(className) { value ->
                            this@CreatePageObjectDialog.className = value
                        }
                    classNameTextField()
                }
            }
            titledRow("Choose your package and destination folder: ") {
                row {
                    val initialPackageName = "com.example.myapplication"

                    packageNameChooserComboBox = PackageNameReferenceEditorCombo(
                        initialPackageName,
                        project,
                        KEY_PACKAGE_NAME_RECENTS,
                        "Choose destination package"
                    )
                    packageNameChooserComboBox()
                }
                row {
                    destinationFolderComboBox = object : KotlinDestinationFolderComboBox() {
                        override fun getTargetPackage(): String {
                            return packageNameChooserComboBox.text.trim()
                        }
                    }
                    destinationFolderComboBox()

                    destinationFolderComboBox.setData(
                        project,
                        xmlFile.containingDirectory,
                        packageNameChooserComboBox.childComponent
                    )
                }
            }
        }
    }

    override fun createCenterPanel(): JComponent? = JPanel(BorderLayout())


    override fun doOKAction() {
        if (isFormValid()) {
            RecentsManager.getInstance(project).registerRecentEntry(KEY_PACKAGE_NAME_RECENTS, getPackageName())
            super.doOKAction()
        }
    }

    fun collectDialogParams(): CreatePageObjectDialogParams {
        return CreatePageObjectDialogParams(
            xmlFile = xmlFile,
            className = getClassName(),
            targetPackageName = getPackageName(),
            targetMoveDestination = getTargetDestinationFolder()!!
        )
    }


    private fun getClassName(): String = className

    private fun getPackageName(): String = packageNameChooserComboBox.text.trim()

    private fun getTargetDestinationFolder(): MoveDestination? {
        val psiManager = PsiManager.getInstance(project)
        val packageWrapper = PackageWrapper(psiManager, getPackageName())

        return destinationFolderComboBox.selectDirectory(packageWrapper, false)
    }

    private fun isFormValid(): Boolean {
        return checkClassNameIsValid()
                && checkPackageNameIsValid() && checkTargetDestinationIsValid() && checkClassNameIsFree()
    }

    private fun checkClassNameIsValid(): Boolean {
        val myClassName = getClassName()

        return when {
            myClassName.isBlank() -> {
                classNameTextField.showErrorDialog(project, "Class name is blank!")
                false
            }

            myClassName.isValidIdentifier(project).not() -> {
                classNameTextField.showErrorDialog(project, "Class name is not valid identifier!")
                false
            }

            else -> {
                true
            }
        }
    }

    private fun checkPackageNameIsValid(): Boolean {
        val myPackageName = getPackageName()

        return when {
            myPackageName.isBlank() -> {
                packageNameChooserComboBox.showErrorDialog(project, "Package name is blank!")
                false
            }

            myPackageName.isValidPackageName(project).not() -> {
                packageNameChooserComboBox.showErrorDialog(project, "Package name is not valid qualified name!")
                false
            }

            else -> {
                true
            }
        }
    }

    private fun checkTargetDestinationIsValid(): Boolean {
        return when (getTargetDestinationFolder()) {
            null -> {
                destinationFolderComboBox.showErrorDialog(project, "Choose destination folder!")
                false
            }

            else -> {
                true
            }
        }
    }

    private fun checkClassNameIsFree(): Boolean {
        val targetPsiDirectory = getTargetDestinationFolder()!!.targetPackage.directories.lastOrNull()!!
        return targetPsiDirectory.checkFileCanBeCreated(fileName = className.toKotlinFileName())
    }

}