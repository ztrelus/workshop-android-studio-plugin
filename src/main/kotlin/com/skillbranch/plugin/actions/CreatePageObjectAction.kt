package com.skillbranch.plugin.actions

import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.codeStyle.CodeStyleManager
import com.intellij.psi.xml.XmlFile
import com.skillbranch.plugin.PluginsConstants
import com.skillbranch.plugin.core.XmlCodeInsightAction
import com.skillbranch.plugin.extensions.psi.collectTagsInfo
import com.skillbranch.plugin.extensions.toKotlinFileName
import com.skillbranch.plugin.services.PageObjectPropertyService
import org.jetbrains.kotlin.idea.core.ShortenReferences
import org.jetbrains.kotlin.idea.formatter.commitAndUnblockDocument
import org.jetbrains.kotlin.idea.util.application.executeWriteCommand
import org.jetbrains.kotlin.psi.KtFile
import org.jetbrains.kotlin.psi.KtPsiFactory


// 1 - Context for Action. \/
// 2 - Dialog: class name + package name \/
// 3 - Dialog verification \/


// 4 -- Collect all XML tags \/
// 5 -- Convert them to properties \/
// 6 -- Get future class text \/
// 7 -- Get PSI of this class \/
// 8 -- Add created file to file system


// Algorithm for generating code
// 1 -- Text \/
// 2 -- PSI == Program Structure Interface \/
// 3 -- Add code into proper place

/**
 * Action for creating Page object from XML layouts.
 */
class CreatePageObjectAction : XmlCodeInsightAction() {

    companion object {
        private const val COMMAND_NAME = "CreatePageObjectActionCommand"
    }

    override fun invoke(project: Project, editor: Editor, psiFile: PsiFile) {
        val dialog = CreatePageObjectDialog(project, psiFile as XmlFile).also { it.show() }
        if (dialog.isOK) {
            handleDialogParams(dialog.collectDialogParams())
        }
    }

    private fun handleDialogParams(params: CreatePageObjectDialogParams) {
        val project = params.xmlFile.project
        val fileText = getFutureFileText(params)

        val ktPsiFactory = KtPsiFactory(project)
        val ktFile = ktPsiFactory.createFile(fileName = params.className.toKotlinFileName(), text = fileText)


        project.executeWriteCommand(COMMAND_NAME) {
            val targetPsiDirectory = params.targetMoveDestination.targetPackage.directories.last()
            val addedFile = targetPsiDirectory.add(ktFile) as KtFile

            // Post processing
            addedFile.commitAndUnblockDocument()
            ShortenReferences.DEFAULT.process(addedFile)
            CodeStyleManager.getInstance(project).reformat(addedFile)
        }
    }

    private fun getFutureFileText(params: CreatePageObjectDialogParams): String {
        val project = params.xmlFile.project

        val tags = params.xmlFile.collectTagsInfo()

        val pageObjectPropertyService = PageObjectPropertyService.newInstance(project)
        val properties = pageObjectPropertyService.convertAndroidTagsInfo(tags)

        return """
        package ${params.targetPackageName}

        class ${params.className} : ${PluginsConstants.AGODA_SCREEN_CLASS_FQNAME}<${params.className}>() {
        
            ${properties.joinToString(separator = "\n")}
        
            val actions = Action()
            val checks = Check()
        
        
            inner class Action : ${PluginsConstants.HH_SCREEN_INTENTIONS_FQNAME}<Action>() {
                // TODO - add your actions
            }
        
            inner class Check : ${PluginsConstants.HH_SCREEN_INTENTIONS_FQNAME}<Check>() {
                // TODO - add your checks
            }
        
        }    
        """
    }

}