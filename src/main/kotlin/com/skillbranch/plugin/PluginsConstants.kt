package com.skillbranch.plugin


object PluginsConstants {

    const val XML_FILE_SUFFIX = ".xml"
    const val KOTLIN_FILE_SUFFIX = ".kt"

    const val AGODA_SCREEN_CLASS_FQNAME = "com.agoda.kakao.screen.Screen"
    const val HH_SCREEN_INTENTIONS_FQNAME = "ru.hh.android.core_tests.page.ScreenIntentions"

}