package com.skillbranch.plugin.model

import com.android.SdkConstants


enum class AndroidViewDeclaration(
    val widgetsClassFQNames: List<String>
) {

    CHECK_BOX(
        widgetsClassFQNames = listOf(
            SdkConstants.FQCN_CHECK_BOX
        )
    ),
    BUTTON(
        widgetsClassFQNames = listOf(
            SdkConstants.FQCN_BUTTON
        )
    ),
    TEXT_VIEW(
        widgetsClassFQNames = listOf(
            SdkConstants.FQCN_TEXT_VIEW
        )
    ),
    VIEW_GROUP(
        widgetsClassFQNames = listOf(
            SdkConstants.CLASS_VIEWGROUP
        )
    ),
    VIEW(
        widgetsClassFQNames = listOf(
            SdkConstants.CLASS_VIEW
        )
    )

}