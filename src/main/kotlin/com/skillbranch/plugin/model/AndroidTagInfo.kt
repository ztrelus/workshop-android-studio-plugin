package com.skillbranch.plugin.model


data class AndroidTagInfo(
    val id: String,
    val androidViewDeclaration: AndroidViewDeclaration
)