package com.skillbranch.plugin.core

import com.intellij.codeInsight.CodeInsightActionHandler
import com.intellij.codeInsight.actions.CodeInsightAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.xml.XmlFile


/**
 * Base class for generating code from XML files.
 */
abstract class XmlCodeInsightAction : CodeInsightAction(), CodeInsightActionHandler {

    final override fun startInWriteAction(): Boolean = false

    final override fun isValidForFile(project: Project, editor: Editor, file: PsiFile): Boolean {
        return file is XmlFile && file.containingDirectory?.name == "layout"
    }


    final override fun getHandler(): CodeInsightActionHandler = this

}