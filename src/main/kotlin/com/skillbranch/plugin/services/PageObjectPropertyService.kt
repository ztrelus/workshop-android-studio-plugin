package com.skillbranch.plugin.services

import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.skillbranch.plugin.model.AndroidTagInfo
import com.skillbranch.plugin.model.AndroidViewDeclaration

@Service
class PageObjectPropertyService {

    companion object {
        fun newInstance(project: Project) = project.service<PageObjectPropertyService>()
    }


    fun convertAndroidTagsInfo(tags: List<AndroidTagInfo>): List<String> {
        return tags.map { tagInfo ->
            val kakaoWidgetFQN = getKakaoWidgetFQName(tagInfo)

            // androidViewDeclaration == RECYCLER_VIEW({withId(R.id.<>})
            "private val ${tagInfo.id} = $kakaoWidgetFQN { withId(R.id.${tagInfo.id}) }"
        }
    }


    private fun getKakaoWidgetFQName(tagInfo: AndroidTagInfo): String {
        return when (tagInfo.androidViewDeclaration) {
            AndroidViewDeclaration.CHECK_BOX -> "com.agoda.kakao.check.KCheckBox"
            AndroidViewDeclaration.BUTTON -> "com.agoda.kakao.text.KButton"
            AndroidViewDeclaration.TEXT_VIEW -> "com.agoda.kakao.text.KTextView"
            AndroidViewDeclaration.VIEW_GROUP, AndroidViewDeclaration.VIEW -> "com.agoda.kakao.common.views.KView"
        }
    }

}